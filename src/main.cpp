#include <memory>
#include "game.h"


int main(int argc, char *argv[]) {
  auto game = std::make_shared<Game>();

  game->start();

  return 1;
}
