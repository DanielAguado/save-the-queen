#ifndef CAMERASLIDER_H
#define CAMERASLIDER_H

#include <OgreSceneNode.h>
#include <OgreCamera.h>
#include <OgreEntity.h>

class Game;

class CameraSlider {
 public:
  static void register_keybindings(std::shared_ptr<Game> game);
  static void unregister_callbacks(std::shared_ptr<Game> game);
  static void move_camera(Ogre::SceneNode* camera_node, Ogre::Vector3 increment);
  static void rotate_camera_x(Ogre::SceneNode* camera, Ogre::Degree degree);
  static void rotate_camera_y(Ogre::SceneNode* camera, Ogre::Degree degree);
  static void rotate_camera_z(Ogre::SceneNode* camera, Ogre::Degree degree);
};


#endif
