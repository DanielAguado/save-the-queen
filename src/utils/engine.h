#ifndef ENGINE_HPP
#define ENGINE_HPP

namespace Engine {
  enum Action {
  		 NONE,
  		 STOP,
       UP,
       DOWN,
       LEFT,
       RIGHT,
       SHOOT,
       ROTATE,
       ROTATE_R,
       ROTATE_L,
       ROTATE_TO_POINT,
       ROTATE_TO_ENEMY,
       SHOOT_TO_ENEMY_OR_FOLLOW_PATH,
       FOLLOW_PATH,
  };
}

#endif