#include "physics.h"

Physics::Physics() {
    btVector3 world_min(-1000,-1000,-1000);
    btVector3 world_max(1000,1000,1000);

    broadphase_ = new btAxisSweep3(world_min,world_max);

    solver_ = new btSequentialImpulseConstraintSolver();
    collision_configuration_ = new btDefaultCollisionConfiguration();
    dispatcher_ = new btCollisionDispatcher(collision_configuration_);

    dynamics_world_ = new btDiscreteDynamicsWorld(dispatcher_,
                        broadphase_, solver_, collision_configuration_);
    dynamics_world_->setGravity(gravity_);
}

Physics::~Physics() {
  delete dynamics_world_;
  delete dispatcher_;
  delete collision_configuration_;
  delete solver_;
  delete broadphase_;
}

btRigidBody*
Physics::create_rigid_body(const btTransform &world_transform,
                  Ogre::SceneNode* node,
                  btCollisionShape* shape,
                  btScalar mass, bool active, bool inertia){
  btVector3 local_inertia(0 ,0 ,0);

  if(mass != 0 && inertia)
    shape->calculateLocalInertia(mass, local_inertia);

  MotionState* motionState = new MotionState(world_transform, node);
  btRigidBody::btRigidBodyConstructionInfo
    rigidBodyCI(mass, motionState, shape, local_inertia);

  btRigidBody* rigidBody = new btRigidBody(rigidBodyCI);
  dynamics_world_->addRigidBody(rigidBody);

  if(active)
    rigidBody->setActivationState(DISABLE_DEACTIVATION);

  return rigidBody;
}

btRigidBody*
Physics::create_rigid_body(btCollisionShape* shape, btVector3 origin, Ogre::SceneNode* node) {
  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btTransform transformation(rotation, origin);

  return create_rigid_body(transformation, node, shape, 0, false);
}

void
Physics::remove_rigid_body(btRigidBody* body) {
  dynamics_world_->removeRigidBody(body);
}

void
Physics::add_body_to_group(btRigidBody* body, std::string group_name) {
  _body_groups[group_name].push_back(body);
}

btCollisionShape*
Physics::create_shape(btVector3 halfExtent){
  return new btBoxShape(halfExtent);
}

btCollisionShape*
Physics::create_shape(float radius){
  return new btSphereShape(radius);
}

btCollisionShape*
Physics::create_shape(MeshStrider* strider){
  return new btBvhTriangleMeshShape(strider, true, true);
}

btCollisionShape*
Physics::create_shape(int radius, int height){
  return new btConeShape(radius, height);
}


btCollisionShape*
Physics::create_shape(btVector3 coordinates,
                      btScalar distance_to_origin) {
  return new btStaticPlaneShape(coordinates, distance_to_origin);
}

btCompoundShape*
Physics::create_compound_shape(btVector3 origin, btCollisionShape* child){
  btCompoundShape* compound = new btCompoundShape();
  btTransform localTrans;
  localTrans.setIdentity();
  localTrans.setOrigin(origin);

  compound->addChildShape(localTrans, child);
  return compound;
}

void
Physics::step_simulation(float deltaT, int maxSubSteps) {
  dynamics_world_->stepSimulation(deltaT, maxSubSteps);
}


void
Physics::add_collision_hooks(Physics::CollisionPair collision_pair,
                             std::function<void()> callback)  {
  if(!triggers_[collision_pair]) {
    triggers_[collision_pair] = callback;
  }
}

void
Physics::add_collision_hooks(btRigidBody* body, std::string group_name,
                             std::function<void()> callback) {
  for(auto& collision_body: _body_groups[group_name])
    add_collision_hooks(Physics::CollisionPair{body, collision_body}, callback);
}

std::vector<btVector3>
Physics::get_positions_of_group(std::string group_name) {
  std::vector<btVector3> positions;
  for(auto& collision_body: _body_groups[group_name])
    positions.push_back(get_position(collision_body));
  return positions;
}

void
Physics::clear_triggers() {
  triggers_.clear();
}

void
Physics::check_collision() {
  int contact_point_caches = dynamics_world_->getDispatcher()->getNumManifolds();
  for (int i = 0; i < contact_point_caches; i++) {
      btPersistentManifold* contact_cache =
        dynamics_world_->getDispatcher()->getManifoldByIndexInternal(i);

      const btCollisionObject* object_a = contact_cache->getBody0();
      const btCollisionObject* object_b = contact_cache->getBody1();

      check_individual_colliders(object_a, object_b);
      check_collision_pairs(object_a, object_b);
    }
}

void
Physics::check_individual_colliders(const btCollisionObject* object_a,
                                    const btCollisionObject* object_b) {
  if(triggers_[Physics::CollisionPair{object_a, nullptr}])
    triggers_[Physics::CollisionPair{object_a, nullptr}]();

  if(triggers_[Physics::CollisionPair{object_b, nullptr}])
    triggers_[Physics::CollisionPair{object_b, nullptr}]();
}

void
Physics::check_collision_pairs(const btCollisionObject* object_a,
                               const btCollisionObject* object_b) {
  if(triggers_[Physics::CollisionPair{object_a, object_b}])
    triggers_[Physics::CollisionPair{object_a, object_b}]();

  if (triggers_[Physics::CollisionPair{object_b, object_a}])
    triggers_[Physics::CollisionPair{object_b, object_a}]();
}

void
Physics::absolute_move(btRigidBody* body, btVector3 position) {
  btTransform transform = body->getCenterOfMassTransform();
  transform.setOrigin(position);
  body->setCenterOfMassTransform(transform);
}

void
Physics::rotate_body(btRigidBody* body, btVector3 rotation_axes, float angle) {
  btTransform transform = body->getCenterOfMassTransform();
  btQuaternion rotation = transform.getRotation();

  btQuaternion delta_rotation(rotation_axes, btScalar(angle));
  btQuaternion new_rotation = delta_rotation * rotation;

  transform.setRotation(new_rotation);
  body->setCenterOfMassTransform(transform);
}

void
Physics::absolute_rotate_body(btRigidBody* body, btVector3 rotation_axes, float angle) {
  btTransform transform = body->getCenterOfMassTransform();
  btQuaternion rotation(rotation_axes, btScalar(angle));

  transform.setRotation(rotation);
  body->setCenterOfMassTransform(transform);
}

void
Physics::set_linear_velocity(btRigidBody* body,
                             btVector3 velocity) {
  body->setLinearVelocity(velocity);
}

void
Physics::stop(btRigidBody* body) {
  body->setLinearVelocity(btVector3(0, 0, 0)); 
  body->setAngularVelocity(btVector3(0, 0, 0));
}

void
Physics::move(btRigidBody* body, btVector3 force) {
  btMatrix3x3& boxRot = body->getWorldTransform().getBasis();
  btVector3 velocity =  body->getLinearVelocity();

  btVector3 correctedForce =  boxRot * force;
  correctedForce += velocity;

  //correctedForce.normalize();
  float max = 10;
  float normalized_force = std::max(std::abs(correctedForce.x()),
                                    std::abs(correctedForce.z()));
  if(normalized_force != 0)
    correctedForce *= max / normalized_force;
  else
    correctedForce = btVector3(0, 0, 0);

  body->setLinearVelocity(correctedForce);
}

btVector3
Physics::get_force_direction(btRigidBody* body, btVector3 force) {
  btMatrix3x3& boxRot = body->getWorldTransform().getBasis();
  return boxRot * force;
}

btVector3
Physics::closest_point(btVector3 point, std::vector<btVector3> vector) {
  btScalar distance(10000);
  btVector3 closest_point;
  int offset = 20;
  for(auto& p: vector) {
    if(point.distance(p) / offset < distance){
      distance = point.distance(p) / offset;
      closest_point = p;
    }
  }
/*
  std::cout << "closest point: " << closest_point.x()
            << ", " << closest_point.y() 
            << ", " << closest_point.z() 
            << std::endl;
*/
  return closest_point;
}

btVector3
Physics::get_position(btRigidBody* body) {
  return body->getCenterOfMassPosition();
}

void
Physics::rotate_to_look_at_point(btRigidBody* body, btVector3 point) {
  btVector3 position = body->getCenterOfMassPosition();
  btVector3 new_direction = (point - position);

  float angle = std::abs(std::atan(new_direction.z() / new_direction.x()));
  angle = new_direction.x() < 0 ? angle + 1.57079632679 : angle;

  btVector3 axis = new_direction.z() < 0 ? btVector3(0, 1, 0) : btVector3(0, -1, 0);

  absolute_rotate_body(body, axis, angle);
}