#ifndef _SOUND_H
#define	_SOUND_H

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include <string>
#include <iostream>
#include <memory>
#include <unistd.h>
#include <chrono>
#include <thread>
#include <map>

class Sound {
 public:
  typedef std::shared_ptr<Sound> shared;

  Sound();
  ~Sound();

  void load(std::string type, std::string fileName);

  void play_music(std::string fileName);
  void pause_music();
  void stop_music();

  void play_fx(std::string fileName);

  bool is_paused();
  bool is_stopped();
  bool is_playing();
  bool in_error_state();

  void enable_playlist();
  void disable_playlist();
  void play_next_track();

 private:

  std::string _current_filename;
  std::map<std::string, Mix_Music*> _musics;
  std::map<std::string, Mix_Chunk*> _fx;

  bool _in_error_state;

  void init_audio_device();
  void play(std::string file_name);

  Mix_Music* acquire_music(std::string fileName);
  std::string next_music(std::string fileName);
  void release_music(std::string fileName);

  Mix_Chunk* acquire_fx(std::string fileName);
  void release_fx(std::string fileName);

  Mix_Music* load_music(std::string file_name);
  Mix_Chunk* load_fx(std::string file_name);
};

#endif
