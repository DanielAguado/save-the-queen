#include "scene.h"

Scene::Scene() {
  Ogre::LogManager* logMgr = OGRE_NEW Ogre::LogManager;
  logMgr->createLog("config/ogre.log", true, false, false);
  _root = new Ogre::Root("config/plugins.cfg", "config/ogre.cfg", "");

  if (not _root->restoreConfig() )
    _root->showConfigDialog();

  _window = _root->initialise(true, "Save the Queen!");

  load_resources();

  _scene_manager = _root->createSceneManager(Ogre::ST_GENERIC);

  create_camera("camera_node", Ogre::Vector3(-40, 133.75, 178.25),
        Ogre::Vector3(0, 0, 5));
  switch_camera(0);

  create_light();

  _ray_query = _scene_manager->createRayQuery(Ogre::Ray());
  _ray_query->setSortByDistance(true);
}

void
Scene::load_resources() {
  Ogre::ConfigFile cf;
  cf.load("config/resources.cfg");

  Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

  Ogre::String secName, typeName, archName;
  while (seci.hasMoreElements()) {
    secName = seci.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typeName = i->first;
      archName = i->second;
      Ogre::ResourceGroupManager::getSingleton()
        .addResourceLocation(archName, typeName, secName);
    }
  }

  Ogre::ResourceGroupManager::getSingleton()
    .initialiseAllResourceGroups();
}

void
Scene::render_one_frame(void) {
  Ogre::WindowEventUtilities::messagePump();
  _root->renderOneFrame();
}

Ogre::Ray
Scene::set_ray_query(float x, float y) {
  Ogre::Ray ray = _camera->
    getCameraToViewportRay(x/float(_window->getWidth()), y/float(_window->getHeight()));

  _ray_query->setRay(ray);

  return ray;
}

void
Scene::create_light() {

  _scene_manager->setAmbientLight(Ogre::ColourValue(0.3, 0.3, 0.3));
  _scene_manager->setShadowTextureCount(2);
  _scene_manager->setShadowTextureSize(512);
  _scene_manager->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5));
  _scene_manager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

  Ogre::Light* light = _scene_manager->createLight("2Light");
  light->setCastShadows(true);
  light->setPosition(100, 1000, 0);
  light->setType(Ogre::Light::LT_POINT);
  light->setDiffuseColour(15, 15 , 15);
  light->setSpecularColour(15, 15 , 15);
}

int
Scene::create_camera(std::string name, Ogre::Vector3 position, Ogre::Vector3 lookAt, Ogre::SceneNode* target_node) {
  _camera = _scene_manager->createCamera(name);
  _camera->setPosition(position);
  _camera->lookAt(lookAt);
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(300);

  Ogre::SceneNode* camera_node = create_node(name);
  camera_node->attachObject(_camera);

  if(target_node != nullptr)
    add_child(target_node, camera_node);
  
  _camera_list.push_back({camera_node, _camera});
  return _camera_list.size() - 1;
}

int
Scene::create_camera(std::string name, Ogre::Vector3 position, Ogre::SceneNode* target_node) {

  _camera = _scene_manager->createCamera(name);
  _camera->setPosition(0, 0, 0);
  _camera->setNearClipDistance(5);
  _camera->setFarClipDistance(300);

  Ogre::SceneNode* camera_node = create_node(name);
  camera_node->attachObject(_camera);
  add_child(target_node, camera_node);

  camera_node->setAutoTracking(true, target_node); 
  camera_node->setFixedYawAxis(true); 
  camera_node->setPosition(position);

  _camera_list.push_back({camera_node, _camera});
  return _camera_list.size() - 1;
}

void
Scene::switch_next_camera() {
  _actual_camera = (_actual_camera + 1) %  _camera_list.size();
  switch_camera(_actual_camera);
}

void
Scene::switch_camera(int index) {
  _actual_camera = index;
  _camera = _camera_list[index].second;

  _window->removeAllViewports();

  Ogre::Viewport* viewport = _window->addViewport(_camera);
  viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

  _camera->setAspectRatio(Ogre::Real(viewport->getActualWidth()) /
                          Ogre::Real(viewport->getActualHeight()));
}

Ogre::SceneNode*
Scene::get_camera_node(int index) {
  return _camera_list[index].first;
}

Ogre::SceneNode*
Scene::create_node(std::string name) {
  return _scene_manager->createSceneNode(name);
}

Ogre::SceneNode*
Scene::get_node(std::string node) {
  if(node == "")
    return _scene_manager->getRootSceneNode();

  if(_scene_manager->hasSceneNode(node))
    return _scene_manager->getSceneNode(node);

  return nullptr;
}

void
Scene::attach(Ogre::SceneNode* node, Ogre::MovableObject* entity) {
  node->attachObject(entity);
}

Ogre::SceneNode*
Scene::create_graphic_element(std::string entity, std::string mesh,
                              std::string parent, std::string name) {
  return create_graphic_element(create_entity(entity, mesh), parent, name);
}

Ogre::SceneNode*
Scene::create_graphic_element(Ogre::Entity* entity, std::string parent, std::string name) {
  Ogre::SceneNode* node = get_child_node(parent, name);
  attach(node, entity);

  return node;
}

Ogre::SceneNode*
Scene::create_plane(std::string axis, std::string name, std::string mesh,
        std::string parent, std::string material) {
  Ogre::Plane planeGround(get_axis(axis), 1);
  Ogre::MeshManager::getSingleton().createPlane(mesh,
  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, planeGround,
  2000, 2000, 200, 200, true, 1, 64, 64, get_normal(axis));

  Ogre::SceneNode* ground_node = get_child_node(parent, name);
  Ogre::Entity* ground_entity = create_entity(name, mesh, false);

  ground_entity->setMaterialName(material);
  ground_node->attachObject(ground_entity);

  return ground_node;
}

Ogre::SceneNode*
Scene::get_child_node(Ogre::SceneNode* parent, std::string name) {
  Ogre::SceneNode* node = get_node(name);
  if(node == nullptr)
      node = create_child_node(parent, name);

  return node;
}

Ogre::SceneNode*
Scene::get_child_node(std::string parent, std::string name) {
  return get_child_node(get_node(parent), name);
}

Ogre::SceneNode*
Scene::create_child_node(Ogre::SceneNode* parent, std::string name) {
  Ogre::SceneNode* node = create_node(name);
  add_child(parent, node);

  return node;
}

Ogre::Entity*
Scene::create_entity(std::string name, std::string mesh, bool cast_shadows ) {
  Ogre::Entity* entity = _scene_manager->hasEntity(name)?
    _scene_manager->getEntity(name): _scene_manager->createEntity(name, mesh);
  entity->setCastShadows(cast_shadows);
  return entity;
}

void
Scene::add_child(std::string parent, std::string child) {
  add_child(get_node(parent), get_node(child));
}

void
Scene::add_child(std::string parent, Ogre::SceneNode* child) {
  add_child(get_node(parent), child);
}

void
Scene::add_child(Ogre::SceneNode* parent, std::string child) {
  add_child(parent, get_node(child));
}

void
Scene::add_child(Ogre::SceneNode* parent, Ogre::SceneNode* child) {
  parent->addChild(child);
}

void
Scene::move_node(std::string node_name, Ogre::Vector3 increment) {
  Ogre::SceneNode* node = get_node(node_name);

  Ogre::Vector3 position = node->getPosition();

  node->setPosition(position + increment);
}

Ogre::ParticleSystem*
Scene::create_particle(std::string node_name, std::string particle_system) {
  return create_particle(get_node(node_name), node_name, particle_system);
}

Ogre::ParticleSystem*
Scene::create_particle(Ogre::SceneNode* node, std::string particle_name, std::string particle_system) {
  if(_scene_manager->hasParticleSystem(particle_name))
    return _scene_manager->getParticleSystem(particle_name);

  Ogre::ParticleSystem* particle =_scene_manager->createParticleSystem(particle_name, particle_system);

  node->attachObject(particle);

  return particle;
}

Ogre::ParticleSystem*
Scene::create_particle(std::string particle_name, std::string particle_system, Ogre::Vector3 position) {
  Ogre::SceneNode* node = get_child_node("", particle_name);
  node->setPosition(position);

  return create_particle(node, particle_name, particle_system);
}

void
Scene::destroy_node(std::string name) {
  destroy_node(get_node(name));
}

void
Scene::destroy_node(Ogre::SceneNode* child) {
  destroy_all_attached_movable_objects(child);
  _scene_manager->destroySceneNode(child);
}

void
Scene::remove_child(std::string parent, std::string child) {
  remove_child(get_node(parent), get_node(child));
}

void
Scene::remove_child(std::string parent, Ogre::SceneNode* child) {
  remove_child(get_node(parent), child);
}

void
Scene::remove_child(Ogre::SceneNode* parent, std::string child) {
  remove_child(parent, get_node(child));
}

void
Scene::remove_child(Ogre::SceneNode* parent, Ogre::SceneNode* child) {
  parent->removeChild(child);
}

void
Scene::destroy_scene() {
  destroy_all_attached_movable_objects(get_node(""));
  get_node("")->removeAndDestroyAllChildren();
}

void
Scene::destroy_all_attached_movable_objects(Ogre::SceneNode* node) {
    for(auto object: node->getAttachedObjectIterator())
       _scene_manager->destroyMovableObject(object.second);

    for(auto child: node->getChildIterator())
        destroy_all_attached_movable_objects(static_cast<Ogre::SceneNode*>(child.second));
}

Ogre::Vector3
Scene::convert_btvector3_to_vector3(btVector3 position){
  return Ogre::Vector3(position.getX(), position.getY(), position.getZ());
}

btVector3
Scene::convert_vector3_to_btvector3(Ogre::Vector3 position){
  return btVector3(position.x, position.y, position.z);
}

Ogre::Vector3
Scene::get_axis(std::string axis){
  if(axis == "X")
    return Ogre::Vector3::UNIT_X;

  if(axis == "Y")
    return Ogre::Vector3::UNIT_Y;

  if(axis == "Z")
    return Ogre::Vector3::UNIT_Z;

  return Ogre::Vector3::UNIT_X;
}

Ogre::Vector3
Scene::get_normal(std::string axis) {
  if(axis == "X")
    return Ogre::Vector3::UNIT_Z;

  if(axis == "Y")
    return Ogre::Vector3::UNIT_X;

  if(axis == "Z")
    return Ogre::Vector3::UNIT_Y;

  return Ogre::Vector3::UNIT_X;
}

void
Scene::create_billboard(std::string billboard_name,
                        std::string material_name,
                        std::vector<Ogre::Vector3> positions,
                        std::string node_name) {
  std::cout << "material: " << material_name << std::endl;
  Ogre::BillboardSet* billboard_set =
    _scene_manager->createBillboardSet(billboard_name, positions.size());

  billboard_set->setSortingEnabled(true);
  billboard_set->setCommonDirection(Ogre::Vector3(0,1,0));
  billboard_set->setBillboardType(Ogre::BBT_ORIENTED_COMMON);
  billboard_set->setMaterialName(material_name);
  billboard_set->setDefaultDimensions(20.,20.);

  for(Ogre::Vector3 position: positions)
    billboard_set->createBillboard(position);

  attach(get_child_node("", node_name), billboard_set);
}
