#ifndef INPUT_HPP
#define INPUT_HPP
#include <map>
#include <vector>
#include <memory>
#include <unistd.h>

#include <OgreRoot.h>
#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>

#include <CEGUI/CEGUI.h>

#include <OIS/OISEvents.h>
#include <OIS/OISInputManager.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>

enum class EventType{repeat, doItOnce};
enum class EventTrigger{ OnKeyPressed, OnKeyReleased};

class EventListener: public Ogre::WindowEventListener,
  public OIS::KeyListener,
  public OIS::MouseListener {

  typedef bool OnKeyPressed;
  typedef std::pair<OIS::MouseButtonID, EventTrigger> MouseKey;
  typedef std::pair<OIS::KeyCode, EventTrigger> KeyBoardKey;

  OIS::InputManager* _input_manager;
  OIS::Mouse* _mouse;
  OIS::Keyboard* _keyboard;

  MouseKey _mouse_key;

  std::map<MouseKey, std::function<void()>> _mouse_triggers;
  std::function<void()> _mouse_movement_trigger;

 public:
  typedef std::shared_ptr<EventListener> shared;
  typedef std::vector<KeyBoardKey> KeyEvents;
  typedef std::vector<MouseKey> MouseEvents;

  float _x, _y, _x_rel, _y_rel;
  bool _exit;

  EventListener(Ogre::RenderWindow* window);

  void capture(void);
  void check_events();

  void add_hook(MouseKey keystroke, EventType type, std::function<void()> callback);
  void add_hook(KeyBoardKey keystroke, EventType type, std::function<void()> callback);
  void add_mouse_movement_hook(std::function<void()> callback);
  void clear_hooks();

  bool keyPressed(const OIS::KeyEvent& arg);
  bool keyReleased(const OIS::KeyEvent& arg);
  bool mouseMoved(const OIS::MouseEvent&  evt);
  bool mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id);
  bool mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id);

  void windowClosed(Ogre::RenderWindow* window);

  bool shutdown(void);
  bool gui_shutdown(const CEGUI::EventArgs &event);

 private:
  KeyEvents _key_events;
  MouseEvents _mouse_events;
  std::map<KeyBoardKey, std::function<void()>> _repeat_key_triggers, _doitonce_key_triggers;
  std::map<MouseKey, std::function<void()>> _repeat_mouse_triggers, _doitonce_mouse_triggers;

  void create_input_manager(Ogre::RenderWindow* window);
  
  void remove_key_from_buffer(KeyBoardKey event);
  void remove_mouse_key_from_buffer(MouseKey event);

  void trigger_keyboard_events();
  void trigger_mouse_events();

  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
  };
#endif