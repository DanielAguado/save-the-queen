#include "input.h"

EventListener::EventListener(Ogre::RenderWindow* window) {
  _x = _y = _x_rel = _y_rel = 0;
  _exit = false;
  _mouse_key.first = OIS::MB_Button7;

  create_input_manager(window);

  _keyboard = static_cast<OIS::Keyboard*>
    (_input_manager->createInputObject(OIS::OISKeyboard, true));
  _mouse = static_cast<OIS::Mouse*>
    (_input_manager->createInputObject(OIS::OISMouse, true));

  _keyboard->setEventCallback(this);
  _mouse->setEventCallback(this);
  Ogre::WindowEventUtilities::addWindowEventListener(window, this);
}

void
EventListener::add_hook(EventListener::KeyBoardKey keystroke,
                        EventType type, std::function<void()> callback) {

  if(type == EventType::repeat
     && !_repeat_key_triggers[keystroke])
      _repeat_key_triggers[keystroke] = callback;
  else if(type == EventType::doItOnce
     && !_doitonce_key_triggers[keystroke]) {
      _doitonce_key_triggers[keystroke] = callback;
  }
}

void
EventListener::add_hook(EventListener::MouseKey keystroke,
                        EventType type, std::function<void()> callback) {
  if(type == EventType::repeat
     && !_repeat_mouse_triggers[keystroke])
      _repeat_mouse_triggers[keystroke] = callback;
  else if(type == EventType::doItOnce
     && !_doitonce_mouse_triggers[keystroke]) {
      _doitonce_mouse_triggers[keystroke] = callback;
  }
}

void
EventListener::add_mouse_movement_hook(std::function<void()> callback) {
  _mouse_movement_trigger = callback;
}

void
EventListener::capture(void) {
    _keyboard->capture();
    _mouse->capture();
}

void
EventListener::check_events(void) {
  if(!_mouse_events.empty())
    trigger_mouse_events();

  if(!_key_events.empty())
    trigger_keyboard_events();
}

void
EventListener::trigger_mouse_events() {
  if(_mouse_events.size() > _doitonce_mouse_triggers.size() + _repeat_mouse_triggers.size())
    return;

  for(auto event: _mouse_events){
    if(_doitonce_mouse_triggers[event]){
      _doitonce_mouse_triggers[event]();
      remove_mouse_key_from_buffer(event);
    }

    if(_repeat_mouse_triggers[event]) {
      _repeat_mouse_triggers[event]();
    }
  }
}

void
EventListener::trigger_keyboard_events() {
  if(_key_events.size() > _doitonce_key_triggers.size() + _repeat_key_triggers.size())
    return;

  for(auto event: _key_events){
    if(_doitonce_key_triggers[event]){
      _doitonce_key_triggers[event]();
      remove_key_from_buffer(event);
    }

    if(_repeat_key_triggers[event]) {
      _repeat_key_triggers[event]();
    }
  }
}

bool
EventListener::shutdown(void) {
    _exit = true;
    return true;
}

void
EventListener::clear_hooks() {
  _mouse_triggers.clear();
  _repeat_key_triggers.clear();
  _doitonce_key_triggers.clear();
  _mouse_movement_trigger = nullptr;
}

bool
EventListener::keyPressed(const OIS::KeyEvent& arg) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().
    getDefaultGUIContext();
  context.injectKeyDown((CEGUI::Key::Scan) arg.key);
  context.injectChar(arg.text);

  remove_key_from_buffer({arg.key, EventTrigger::OnKeyReleased});
  _key_events.push_back({arg.key, EventTrigger::OnKeyPressed});
  return true;
}

bool
EventListener::keyReleased(const OIS::KeyEvent& arg) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().
    getDefaultGUIContext();
  context.injectKeyUp((CEGUI::Key::Scan)arg.key);

  remove_key_from_buffer({arg.key, EventTrigger::OnKeyPressed});
  _key_events.push_back({arg.key, EventTrigger::OnKeyReleased});
   return true;
}

bool
EventListener::mouseMoved(const OIS::MouseEvent& evt) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
  context.injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
  
  if(!_mouse_movement_trigger) 
    return true;
  _x_rel = evt.state.X.rel;
  _y_rel = evt.state.Y.rel;
  _mouse_movement_trigger();
  OIS::MouseState &mutableMouseState = const_cast<OIS::MouseState &>(_mouse->getMouseState());
  mutableMouseState.X.abs = 100;
  mutableMouseState.Y.abs = 100;
  
  return true;
}

bool
EventListener::mousePressed(const OIS::MouseEvent& evt,
                            OIS::MouseButtonID id) {
      CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseButtonDown(convertMouseButton(id));
  _x = evt.state.X.abs;
  _y = evt.state.Y.abs;

  remove_mouse_key_from_buffer({id, EventTrigger::OnKeyPressed});
  _mouse_events.push_back({id, EventTrigger::OnKeyPressed});
  return true;
}

bool
EventListener::mouseReleased(const OIS::MouseEvent& evt,
                             OIS::MouseButtonID id) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
  context.injectMouseButtonUp(convertMouseButton(id));

  remove_mouse_key_from_buffer({id, EventTrigger::OnKeyPressed});
  _mouse_events.push_back({id, EventTrigger::OnKeyPressed});
  return true;
}

void
EventListener::windowClosed(Ogre::RenderWindow* window){
  _exit = true;
}

void
EventListener::create_input_manager(Ogre::RenderWindow* window) {
  typedef std::pair<std::string, std::string> parameter;
    OIS::ParamList parameters;
    size_t xid = 0;

    window->getCustomAttribute("WINDOW", &xid);
    parameters.insert(parameter("WINDOW", std::to_string(xid)));
    
    parameters.insert(parameter("x11_mouse_grab", "false"));
    parameters.insert(parameter("x11_mouse_hide", "false"));
    parameters.insert(parameter("x11_keyboard_grab", "false"));
    parameters.insert(parameter("XAutoRepeatOn", "false"));

    _input_manager = OIS::InputManager::createInputSystem(parameters);
}

void
EventListener::remove_key_from_buffer(KeyBoardKey event) {
    auto keyevent = find (_key_events.begin(), _key_events.end(), event);
    if(keyevent == _key_events.end())
      return;
    _key_events.erase(keyevent);
}

void
EventListener::remove_mouse_key_from_buffer(MouseKey event) {
    auto mouseevent = find (_mouse_events.begin(), _mouse_events.end(), event);
    if(mouseevent == _mouse_events.end())
      return;
    _mouse_events.erase(mouseevent);
}

CEGUI::MouseButton
EventListener::convertMouseButton(OIS::MouseButtonID id) {
  CEGUI::MouseButton cegui_id;
  switch(id)
    {
    case OIS::MB_Left:
      cegui_id = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      cegui_id = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      cegui_id = CEGUI::MiddleButton;
      break;
    default:
      cegui_id = CEGUI::LeftButton;
    }
  return cegui_id;
}

bool
EventListener::gui_shutdown(const CEGUI::EventArgs &event) {
    _exit = true;
    return true;
}