#ifndef PHYSICS_H
#define PHYSICS_H
#include "motionstate.h"
#include "meshstrider.h"
#include <OgreSceneNode.h>
#include <btBulletDynamicsCommon.h>

class Physics {
 public:
  typedef std::pair<const btCollisionObject*, const btCollisionObject*> CollisionPair;
  typedef std::map<CollisionPair, std::function<void()>> Triggers;
  typedef std::shared_ptr<Physics> shared;

  btDiscreteDynamicsWorld* dynamics_world_;

  Physics();
  virtual ~Physics();
  btRigidBody* create_rigid_body(const btTransform &world_transform,
                                 Ogre::SceneNode* node,
                                 btCollisionShape* shape,
                                 btScalar mass, bool active = true, 
                                 bool inertia = false);
  btRigidBody* create_rigid_body(btCollisionShape* shape, btVector3 origin, Ogre::SceneNode* node);
  void remove_rigid_body(btRigidBody* body);

  void set_linear_velocity(btRigidBody* body, btVector3 velocity);
  void stop(btRigidBody* body);

  void absolute_move(btRigidBody* body, btVector3 position);
  void move(btRigidBody* body, btVector3 force);

  void rotate_body(btRigidBody* body, btVector3 rotation_axes, float angle);
  void absolute_rotate_body(btRigidBody* body, btVector3 rotation_axes, float angle);
  void rotate_to_look_at_point(btRigidBody* body, btVector3 point);

  std::vector<btVector3> get_positions_of_group(std::string group_name);
  btVector3 get_position(btRigidBody* body);
  btVector3 get_force_direction(btRigidBody* body, btVector3 force);
  btVector3 closest_point(btVector3 point, std::vector<btVector3> vector);

  btCollisionShape* create_shape(btVector3 halfExtent);
  btCollisionShape* create_shape(float radius);
  btCollisionShape* create_shape(MeshStrider* strider);
  btCollisionShape* create_shape(btVector3 coordinates, btScalar distance_to_origin);
  btCollisionShape* create_shape(int radius, int height);
  btCompoundShape* create_compound_shape(btVector3 origin, btCollisionShape* child);

  void add_collision_hooks(CollisionPair key, std::function<void()> callback);
  void add_collision_hooks(btRigidBody* body, std::string group_name,
                             std::function<void()> callback);
  void clear_triggers();

  void step_simulation(float deltaT, int maxSubSteps);
  void check_collision();

  void add_body_to_group(btRigidBody* body, std::string group_name = "default");

 private:
  btBroadphaseInterface* broadphase_;
  btSequentialImpulseConstraintSolver* solver_;
  btDefaultCollisionConfiguration* collision_configuration_;
  btCollisionDispatcher* dispatcher_;

  const btVector3 gravity_ = btVector3(0, -100, 0);
  Triggers triggers_;

  std::map<std::string, std::vector<btRigidBody*>> _body_groups;

  void check_collision_pairs(const btCollisionObject* object_a, const btCollisionObject* object_b);
  void check_individual_colliders(const btCollisionObject* object_a, const btCollisionObject* object_b);
};

#endif
