#include "game.h"

//#define PHYSICS_DEBUG_DRAWER

Game::Game() {
  _scene = std::make_shared<Scene>();
  _physics = std::make_shared<Physics>();
  _input = std::make_shared<EventListener>(_scene->_window);
  _gui = std::make_shared<GUI>();
  _sound = std::make_shared<Sound>();
  _animation = std::make_shared<Animation>();

  _player = std::make_shared<Player>();

  #ifdef PHYSICS_DEBUG_DRAWER
  _debug_drawer = new PhysicsDebugDrawer(
                  _scene->_scene_manager->getRootSceneNode(), 
                  _physics->dynamics_world_);
  #endif
}

Game::~Game() {
  delete _debug_drawer;
}

void
Game::start() {
  _state_table["play"] = std::make_shared<Play>(shared_from_this());
  _state_table["pause"] = std::make_shared<Pause>(shared_from_this());
  _state_table["menu"] = std::make_shared<Menu>(shared_from_this());
  _state_table["results"] = std::make_shared<Results>(shared_from_this());

  load_sound();
  set_current_state("menu");

  #ifdef PHYSICS_DEBUG_DRAWER
  enable_physics_debug_drawer();
  #endif

  game_loop();
}

void
Game::set_current_state(std::string next_state) {
  _current_state = next_state;
  _state_table[next_state]->init();
}

bool
Game::has_ended() {
  return _player->has_lose() || _player->has_win();
}

void
Game::load_sound() {
  
  _sound->load("music", "media/sound/menu.wav");
  _sound->load("fx", "media/sound/win.wav");
  _sound->load("fx", "media/sound/explosion.wav");
}

void
Game::game_loop() {
  _timer.start();
  while(!_input->_exit) {
    _delta += _timer.get_delta_time();
    _input->capture();
    if(_delta >= (1/FPS)) {
      _input->check_events();
      _state_table[_current_state]->update();
      _scene->render_one_frame();
      
      #ifdef PHYSICS_DEBUG_DRAWER
      _debug_drawer->step();
      #endif
      
      _delta = 0.f;
    }
  }
}

void
Game::enable_physics_debug_drawer() {
  _debug_drawer->setDebugMode(btIDebugDraw::DBG_DrawWireframe | btIDebugDraw::DBG_DrawAabb |
                    btIDebugDraw::DBG_DrawContactPoints);                

  _physics->dynamics_world_->setDebugDrawer(_debug_drawer);
}