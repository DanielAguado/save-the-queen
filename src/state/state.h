#ifndef STATE_HPP
#define STATE_HPP
#include <memory>

#include "scene.h"
#include "input.h"
#include "sound.h"

class Game;

class State {


public:
  typedef std::shared_ptr<State> shared;

  std::shared_ptr<Game> _game;  

  State(std::shared_ptr<Game> game);
  virtual ~State();

  virtual void init() = 0;
  virtual void exit() = 0;
  virtual void update() = 0;
};

#endif
