#include "game.h"

Play::Play(Game::shared game): State(game) {
  _init = false;
  _delay_resume = 0;
  _session = std::make_shared<Session>(game);
}

Play::~Play() {
}

void
Play::init() {
  if(!_init){
    _session->initialize();
    _init = true;
  }

  if(!_session->_paused)
    _session->reset();

  else
    _delay_resume = 0.2;
  
  add_callbacks();
}

void
Play::exit() {
  _game->_input->clear_hooks();
  _game->_sound->pause_music();
}

void
Play::pause() {
  _session->pause();
  exit();
  State::_game->set_current_state("pause");
}

void
Play::end_game() {
  //if(!_game->has_ended()) return;
  _session->_finished = false;
  _session->pause();
  _session->_paused = false;

  exit();
  State::_game->set_current_state("results");
}

void
Play::add_callbacks() {
  //_game->_input->add_hook({OIS::KC_ESCAPE, EventTrigger::OnKeyPressed}, EventType::doItOnce,
  //                        std::bind(&EventListener::shutdown, _game->_input));
  _game->_input->add_hook({OIS::KC_ESCAPE, EventTrigger::OnKeyPressed},
                          EventType::doItOnce,
                          std::bind(&Play::pause, this));
  _session->register_keybindings();
}

void
Play::update() {
  float deltaT = _game->_delta;
  _session->update(deltaT);

  _delay_resume -= deltaT;
  if(_delay_resume < 0) {
    _session->resume();
    _delay_resume = 1000000;
  }

  if(_session->_finished)
    end_game();
}
