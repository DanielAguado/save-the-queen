#ifndef MENU_H
#define MENU_H
#include "state.h"
#include "file_manager.h"


class Menu: public State {
  CEGUI::Window *_menu_window, *_record_window, *_credits_window;
  Ogre::Overlay* _background;

public:
  Menu(std::shared_ptr<Game> game);
  virtual ~Menu();

 void init();
 void exit();
 void update();

 private:
 bool change_state(const CEGUI::EventArgs &event);
 bool switch_records(const CEGUI::EventArgs &event);
 bool switch_credits(const CEGUI::EventArgs &event);
 void add_hooks();
 void add_hooks(CEGUI::Window *window, const std::string& button,
                const CEGUI::Event::Subscriber& callback);
 void add_gui_hooks();
 void init_gui();
 void load_windows();
 void get_records();
};


#endif
