#ifndef IA_CONTROLLER_H
#define IA_CONTROLLER_H

#include <cmath>

#include "controller.h"

class AIController: public Controller {
 private:
  float _time_from_start, _time_shoot;
  int _delay_start, _delay_shoot;

 public:
  AIController(Player::shared player, int delay = 0);

  virtual ~AIController();

  void configure();
  void exec(Engine::Action action);
  void update(float deltaT);

};

#endif