#ifndef HUMAN_CONTROLLER_H
#define HUMAN_CONTROLLER_H

#include <map>

#include "controller.h"
#include "input.h"

class HumanController: public Controller {
 public:
  typedef std::map<OIS::KeyCode, std::pair<Engine::Action, Engine::Action>> KeyBinding;
  typedef std::map<OIS::MouseButtonID, Engine::Action> MouseBinding;

  HumanController(EventListener::shared listener,
                  Player::shared player);

  virtual ~HumanController();

  void configure();
  void exec(Engine::Action action);
  void update(float deltaT);

 private:
  EventListener::shared _input;

  KeyBinding _key_bindings;
  MouseBinding _mouse_bindings;

  void assign_actions();
};

#endif