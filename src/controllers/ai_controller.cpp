#include "ai_controller.h"

AIController::AIController (Player::shared player, int delay) :
                            Controller(player) {
  _delay_start = delay;
  _delay_shoot = 2;

  _time_from_start = 0;
  _time_shoot = 0;
  
  configure();
}

AIController::~AIController() {

}

void
AIController::configure() {
  std::cout << "configuring IA controller" << std::endl;
}

void
AIController::update(float deltaT) {
  _player->update(deltaT);
  _time_from_start += deltaT;
  _time_shoot += deltaT;

  exec(Engine::Action::ROTATE_TO_ENEMY);
  if(_player->_near_to_enemy) {
    if(_time_shoot > _delay_shoot) {
      exec(Engine::Action::SHOOT);
      _time_shoot = 0;
    }
  }
  else {
    exec(Engine::Action::FOLLOW_PATH);
    exec(Engine::Action::UP);
  }
}

void
AIController::exec(Engine::Action action) {
  if (_time_from_start < _delay_start) return;
  _player->exec(action);
}