#include "human_controller.h"

HumanController::HumanController(EventListener::shared listener,
                                 Player::shared player) :
                                 Controller(player) {

  _input = listener;
  configure();
}

HumanController::~HumanController() {

}

void
HumanController::assign_actions() {
  _key_bindings = {
    {OIS::KC_W,          {Engine::Action::UP, Engine::Action::STOP}},
    {OIS::KC_S,          {Engine::Action::DOWN, Engine::Action::STOP}},
    {OIS::KC_A,          {Engine::Action::LEFT, Engine::Action::STOP}},
    {OIS::KC_D,          {Engine::Action::RIGHT, Engine::Action::STOP}}
  };

  _mouse_bindings = {
    {OIS::MB_Left,        Engine::Action::SHOOT},
    {OIS::MB_Button3,     Engine::Action::ROTATE}
  };
}

void
HumanController::configure() {
  std::cout << "configuring human controller" << std::endl;
  assign_actions();

  for(auto key_bind: _key_bindings) {
    _input->add_hook({key_bind.first, EventTrigger::OnKeyPressed}, EventType::repeat,
                     std::bind((void(HumanController::*)
                     (Engine::Action))(&HumanController::exec), this, key_bind.second.first));
    _input->add_hook({key_bind.first, EventTrigger::OnKeyReleased}, EventType::doItOnce,
                     std::bind((void(HumanController::*)
                     (Engine::Action))(&HumanController::exec), this, key_bind.second.second));
  }

  for(auto mouse_bind: _mouse_bindings) {
    if(mouse_bind.first == OIS::MB_Button3) 
      _input->add_mouse_movement_hook(std::bind((void(HumanController::*)
                     (Engine::Action))(&HumanController::exec), this, mouse_bind.second));
    else
      _input->add_hook({mouse_bind.first, EventTrigger::OnKeyPressed}, EventType::doItOnce,
                     std::bind((void(HumanController::*)
                     (Engine::Action))(&HumanController::exec), this, mouse_bind.second));
  }
}

void
HumanController::update(float deltaT) {
  _player->update(deltaT);
}

void
HumanController::exec(Engine::Action action) {
  _player->exec(action);
}