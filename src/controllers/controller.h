#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <memory>

#include "engine.h"
#include "player.h"

class Controller {
 public:
 	typedef std::shared_ptr<Controller> shared;

  Player::shared _player;

  Controller(Player::shared player);
  virtual ~Controller();

  virtual void configure() = 0;
  virtual void update(float deltaT) = 0;
  virtual void exec(Engine::Action action) = 0;

  void add_collision_hooks();
};
#endif
