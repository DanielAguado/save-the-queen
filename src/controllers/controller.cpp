#include "controller.h"

Controller::Controller(Player::shared player) {
	_player = player;
}

Controller::~Controller() {

}

void
Controller::add_collision_hooks() {
	_player->add_collision_hooks();
}