 #include "player.h"

Player::Player() {
  _level = 0;
  _ball_index = 0;
  _number_of_balls = 10;
  _died = false;
  _is_attacking = false;
  defrost();

  for (int i = 0; i < _number_of_balls; i++)
    _balls.push_back(std::make_shared<Ball>());
}

Player::~Player() {

}

void
Player::initialize(Scene::shared scene, Physics::shared physics,
                   Sound::shared sound, EventListener::shared input,
                   Animation::shared animation, GUI::shared gui,
                   btVector3 origin, std::string name,
                   std::vector<btVector3> way_points,
                   int max_respawn_times) {
  _scene = scene;
  _physics = physics;
  _sound = sound;
  _input = input;
  _animation = animation;
  _gui = gui;

  _origin = origin;
  _way_points = way_points;
  _name = name;
  _max_respawn_times = max_respawn_times;
  _left_respawn_times = _max_respawn_times;
  _level = 0;

  _near_to_enemy = false;
  _init = _name == "queen" ? true : false;
  _shoot_delay = 0;
  _lose_life_delay = 0;
  _respawn_delay = 0;
  
  _index = 0;

  _max_health_points = _name == "player" ? 4 : 2;
  _health_points = _max_health_points;
  _offset = _name == "player" ? 10 : 50;

  initialize_character();
  initialize_balls();

  add_action_hooks();

  if (_name != "player") return;
  create_traking_cameras();
  _power_bar = _gui->load_layout("Session.layout");
  update_health_bar(1);
}

void
Player::add_collision_hooks() {
  for(auto& ball: _balls)
    ball->add_collision_hooks();

  if (_name == "player" || _name == "queen")
    _physics->add_collision_hooks(_body, "enemy_collisions",
                                std::bind(&Player::lose_life, this));
  else 
    _physics->add_collision_hooks(_body, "player_collisions",
                                std::bind(&Player::lose_life, this));
}

void
Player::add_action_hooks() {
  _action_hooks[Engine::Action::UP] = std::bind(&Player::move_up, this);
  _action_hooks[Engine::Action::DOWN] = std::bind(&Player::move_down, this);
  _action_hooks[Engine::Action::LEFT] = std::bind(&Player::move_left, this);
  _action_hooks[Engine::Action::RIGHT] = std::bind(&Player::move_right, this);
  _action_hooks[Engine::Action::STOP] = std::bind(&Physics::stop, _physics, _body);
  
  _action_hooks[Engine::Action::SHOOT] = std::bind(&Player::shoot_weapon, this);
  
  _action_hooks[Engine::Action::ROTATE] = std::bind(&Player::rotate_from_input, this);

  _action_hooks[Engine::Action::ROTATE_R] = std::bind(&Player::rotate_horizontal, this,
                                  -10.471975512); //Radians * 0.05 to turn 180º in 1 second
  _action_hooks[Engine::Action::ROTATE_L] = std::bind(&Player::rotate_horizontal, this,
                                  10.471975512);
  _action_hooks[Engine::Action::ROTATE_TO_ENEMY] = std::bind(&Player::rotate_to_enemy, this);

  _action_hooks[Engine::Action::FOLLOW_PATH] = std::bind(&Player::follow_path, this);
}

void
Player::initialize_character() {
  std::string mesh_name = "Golem.mesh";
  if (_name == "queen")
    mesh_name = "Daisy.mesh";

  //Ogre::Entity* entity =  _scene->create_entity(_name, "character.mesh");
  Ogre::Entity* entity =  _scene->create_entity(_name, mesh_name);
  _node = _scene->create_graphic_element(entity, "", _name);

  btCollisionShape* shape = _physics->create_shape(btVector3(0.5, 0.9, 0.5));

  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btVector3 no_init_position = (_name == "queen") ? 
                                _origin : btVector3(-1, 2, -_offset);

  btTransform transformation(rotation, no_init_position);

  btScalar mass(50);
  _body = _physics->create_rigid_body(transformation, _node, shape, mass);

  _body->setRestitution(0.1);
  _body->setLinearFactor(btVector3(1, 1, 1));
  _body->setAngularFactor(btVector3(0, 1, 0));

  if (_name == "player" || _name == "queen") {
    _physics->add_body_to_group(_body, "player_collisions");
    _physics->add_body_to_group(_body, "player");
  }
  else {
    _physics->add_body_to_group(_body, "enemy_collisions");
    _physics->add_body_to_group(_body, "enemy"); 
  }

  if (_name != "queen") {
    _walking = _animation->create_animation(entity, "Walk", true);
    _attacking = _animation->create_animation(entity, "Throw_Rock", false);
    _dead = _animation->create_animation(entity, "Death", false);
  }
  /*
  else
    _animation->activate(entity, "Idle", true);
    */
}

void
Player::initialize_balls() {
  for(auto& ball: _balls) {
    ball->initialize(_scene, _physics, _sound,
          btVector3(-_ball_index, 2, -_offset), _name);
    _ball_index = (_ball_index + 1) % _number_of_balls;
  }
}

void
Player::create_traking_cameras() {
	_first_person_camera = _scene->get_camera_node(_scene->create_camera("first_person_camera",
													Ogre::Vector3(1, 1.7, 1), Ogre::Vector3(7, 1.7, 0), _node));
	_third_person_camera = _scene->get_camera_node(_scene->create_camera("third_person_camera", 
													Ogre::Vector3(-13, 1.7, 1.5),  Ogre::Vector3(7, 1.7, 1.5), _node));
}

void
Player::reset() {
  if (!_init) return;
  _died = false;
  _left_respawn_times = _max_respawn_times;
  respawn();
}

void
Player::frost() {
  _frozen = true;
}

void
Player::defrost() {
  _frozen = false;
}

void
Player::respawn() {
  if(!_init) return;
  std::cout << _name << " " << __func__ << std::endl;
  if(_died) {
    _physics->absolute_move(_body, 
              _origin + btVector3(-1000, 0, 0));
    return;
  }

  _is_attacking = false;
  if (_name != "queen") {
    _attacking->setTimePosition(0);
    _dead->setTimePosition(0);
  }
  _index = 0;  
  defrost();
  _health_points = _max_health_points;
  _physics->absolute_move(_body, _origin);
  clear_counters();
}

void
Player::lose_life() {
  if(_lose_life_delay > 0) return;
  _lose_life_delay = 2;
  _health_points--;
  std::cout << _name << " health_points: " << _health_points << std::endl;

  if(_health_points <= 0) {
    _left_respawn_times--;
    _respawn_delay = 2;
    frost();
    if(_name != "queen")
      _dead->setTimePosition(0);
    if(_left_respawn_times < 0)
      _died = true;
  }

  if(_name == "player") 
    update_health_bar((float) _health_points / (float) _max_health_points);
}

void
Player::clear_counters(){
  _time_counter = 0.0;
  _score = 0;
}

void
Player::exec(Engine::Action action) {
  if(_died || _frozen) return;
  if(!_init) {
    _init = true;
    respawn();
  }
  _action_hooks[action]();
}

void
Player::move_up() {
  if(_name != "queen")
    _walking->addTime(_deltaT * 3);
  move(btVector3(10, 0, 0));
}

void
Player::move_down() {
  if(_name != "queen")
    _walking->addTime(- _deltaT * 3);
  move(btVector3(-10, 0, 0));
}

void
Player::move_right() {
  move(btVector3(0, 0, 10));
}

void
Player::move_left() {
  move(btVector3(0, 0, -10));
}

void
Player::move(btVector3 orientation) {
  _physics->move(_body, orientation);
}

void
Player::rotate_to_point(btVector3 point) {
  _physics->rotate_to_look_at_point(_body, point);
}

void
Player::rotate_to_enemy() {
  std::vector<btVector3> positions = _name == "player" ? 
                _physics->get_positions_of_group("enemy") :
                _physics->get_positions_of_group("player");
  btVector3 my_position = _physics->get_position(_body);


  int offset = 20;
  int influence_area = 2;
  int distance = 0;
  for(auto& enemy_position : positions) {
    distance = my_position.distance(enemy_position) / offset;
    if(distance < influence_area) {
      rotate_to_point(enemy_position);
      _near_to_enemy = true;
      return;
    }
  }
  
  /*
  int influence_area = 5;
  int distance = 0;
  for(auto& enemy_position : positions) {
    distance = my_position.distance(enemy_position);
    if(distance < influence_area) {
      rotate_to_point(enemy_position);
      _near_to_enemy = true;
      return;
    }
  }
  */

  _near_to_enemy = false;
}

void
Player::follow_path() {
  if(_way_points.size() == 0)
    std::cout << "No way for " << _name << " character" << std::endl;

  btVector3 position = _physics->get_position(_body);
  _physics->rotate_to_look_at_point(_body, _way_points[_index]);

  int distance = std::abs(position.x() - _way_points[_index].x()) +
                 std::abs(position.z() - _way_points[_index].z());
  distance = distance / 20;
  if (distance < 1) 
    _index = (_index + 1) < (int) _way_points.size() ? _index + 1 : _index;
}

void
Player::rotate_from_input() {
  rotate(_input->_x_rel, _input->_y_rel);
}

void
Player::rotate_horizontal(float x) {
  rotate(x, 0);
}

void
Player::rotate(float x, float y) {

  float sensitivity = 0.005;
  if (y != 0) {
    Ogre::Radian angle(- y * sensitivity);
    Ogre::Radian absolute_angle = _first_person_camera->getOrientation().getRoll();

    Ogre::Radian max_angle(0.08);
    Ogre::Radian min_angle(-0.3);
    Ogre::Radian angle_after = angle + absolute_angle;

    if (angle_after < max_angle && 
          angle_after > min_angle) {
      _first_person_camera->roll(angle);
      _third_person_camera->roll(angle);
    }
  }

  if(x == 0)
    return; 
  
  sensitivity *= 10;
  float angle2 = x > 0 ? sensitivity : - sensitivity;
  _physics->rotate_body(_body, btVector3(0, 1, 0), angle2);
}

bool
Player::has_lose(){
  return _died;
}

bool
Player::has_win() {
  return false;
}

void
Player::shoot_weapon() {
  if(_is_attacking) return;

  if(_name != "queen") {
    _is_attacking = true;
    _attacking->setTimePosition(0);
  }
}

void
Player::shoot_callback() {
  if(_shoot_delay > 0) return;

  Ogre::Vector3 character_position = _node->getPosition();
  //character_position += Ogre::Vector3(0, 1, 0);

  btVector3 force(5000, 0, 0);
  force = _physics->get_force_direction(_body, force);

  _balls[_ball_index]->shot(character_position, force);
  _ball_index = (_ball_index + 1) % _number_of_balls;
  _shoot_delay = 0.2;
}

void
Player::update(float deltaT) {
  _deltaT = deltaT;
  for (auto& ball: _balls)
    ball->update();

  _time_counter += deltaT;

  _shoot_delay -= deltaT;
  _lose_life_delay -= deltaT;
  _respawn_delay -= deltaT;

  if (_frozen && _name != "queen")
    _dead->addTime(_deltaT * 3);

  if (_is_attacking) {
    _attacking->addTime(_deltaT * 10);
    if(_attacking->hasEnded()) { 
      _is_attacking = false;
      shoot_callback();
    }
  }

  if(_respawn_delay < 0) {
    respawn();
    _respawn_delay = 1000000;
  }
}

void
Player::update_health_bar(float value) {
  CEGUI::ProgressBar* progress =
    static_cast<CEGUI::ProgressBar*>( _power_bar->getChild("PowerBar"));
    progress->setProgress(value);
}

std::string
Player::to_string() {
  std::stringstream string;
  string << "Level: " << _level
         << " Time: " << to_string_with_precision(_time_counter, 4)
         << " Score: " << _score
         << "\n";

  return string.str();
}

std::string
Player::to_string_with_precision(const float number, const int n) {
    std::ostringstream number_stringify;
    number_stringify << std::setprecision(n) << number;
    return number_stringify.str();
}

std::string
Player::time_to_string() {
  return to_string_with_precision(_time_counter, 4);
}