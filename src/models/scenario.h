#ifndef SCENARIO_H
#define SCENARIO_H

#include <iostream>
#include <fstream>
#include <string>
#include <stack>
#include <map>
#include <functional>
#include <uchar.h>

#include "physics.h"
#include "scene.h"
#include "sound.h"
#include "animation.h"
#include "meshstrider.h"

class Scenario {
  int _x, _z, _offset;

	std::string _scenario_filename;
  std::wifstream _file;

  std::vector<Ogre::SceneNode*> _scenario_nodes;
  std::vector<std::pair<btRigidBody*, btCollisionShape*>> _scenario_bodies;

	std::vector<std::wstring> _scenario;
	std::map<wchar_t, std::function<void()>> _builder;
	Scene::shared _scene;
  Physics::shared _physics;
	Animation::shared _animation;
  Sound::shared _sound;

  enum Orientation {VERTICAL, HORIZONTAL, DOWN_RIGHT, DOWN_LEFT, UP_RIGHT, UP_LEFT};

 public:
  typedef std::shared_ptr<Scenario> shared;

  btVector3 _queen_position;
  std::vector<btVector3> _player_positions;
  std::vector<btVector3> _enemy_sources_positions;
  std::map<int, std::vector<btVector3>> _distace_tree;

  int _number_of_enemies;

  Scenario();
  Scenario(Scene::shared scene, Physics::shared physics,
           Sound::shared sound, Animation::shared animation,
           std::string scenario_filename);
  ~Scenario();

  void load();
  void clean();
  void build_next(std::string file);
  void add_collision_hooks(btRigidBody* body, std::function<void()> callback);

  bool scenenario_finished();

  void create_piece(std::string mesh, Scenario::Orientation orientation);
 private:
	void open_file();
	void read_file();
	void parse();

  void calculate_distance_tree();
  void add_default_scenery();
  void add_entity(std::string name, std::string mesh,
                  Ogre::Vector3 position, float scale = 1.f);
};
#endif
