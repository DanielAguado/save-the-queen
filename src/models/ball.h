#ifndef BALL_
#define BALL_HPP
#include <memory>
#include <cmath>

#include <OgreEntity.h>
#include <OgreSceneNode.h>

#include "physics.h"
#include "sound.h"
#include "scene.h"
#include "gui.h"

class Ball {
  Physics::shared _physics;
  GUI::shared _gui;
  Sound::shared _sound;
  Scene::shared _scene;

  std::string _name;
  btVector3 _origin;
  btVector3 _force;
  bool _shooting;

 public:
  typedef std::shared_ptr<Ball> shared;
  btRigidBody* _body;

  Ball();
  ~Ball();

  void initialize(Scene::shared scene, Physics::shared physics,
                  Sound::shared sound,
                  btVector3 origin, std::string name);
  void add_collision_hooks();
  void reset();
  void shot(Ogre::Vector3 position, btVector3 force);

  void update();
  void collide();
};
#endif