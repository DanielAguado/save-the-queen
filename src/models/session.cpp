#include "session.h"
#include "game.h"

Session::Session(Game::shared game) {
  _game = game;
  _physics = _game->_physics;
  _player = _game->_player;
  _queen = std::make_shared<Player>();

  _camera_index = 2;
  _started = false;
  _paused = false;
  _finished = false;
}

Session::~Session() {
  _game->_input->clear_hooks();
  _game->_scene->destroy_scene();
  _game->_animation->clear();
}

void
Session::initialize() {
  initialize_scenario();

  initialize_enemies();
  initialize_player();
  initialize_queen();

  add_collision_hooks();
  show_interface();
  
  _game->_scene->switch_camera(_camera_index);
  _started = true;
  _finished = false;
}

void
Session::resume() {
  _paused = false;
  _player->defrost();
  _player->_power_bar->setVisible(true);
  _human_controller->configure();
  for(auto& controller: _no_human_controllers)
    controller->_player->defrost();
}

void
Session::reset() {
  _player->reset();
  _queen->reset();
  _human_controller->configure();
  for(auto& controller: _no_human_controllers)
    controller->_player->reset();
}

void
Session::initialize_overlays() {
  _timer_overlay = _game->_gui->create_overlay("Info", "Timer");
}

void
Session::clean_session() {
  reset_overlays();
}

void
Session::reset_overlays(){
  _timer_overlay->hide();

  _timer_overlay->setCaption("");
}

void
Session::show_overlays() {
  if(!_started)
    return;

  _timer_overlay->show();
}

void
Session::update_overlays(){
  if (!_started)
    return;

  _timer_overlay->
    setCaption("Time: " + _player->time_to_string());
}

void
Session::initialize_scenario() {
  std::stringstream level;
  level << "media/scenarios/level" << _player->_level << ".txt";
  //level << "media/scenarios/level0" << ".txt";
  _scenario = std::make_shared<Scenario>(_game->_scene, _physics,
                   _game->_sound, _game->_animation, level.str());
}

void
Session::initialize_player() {
  btVector3 position = _scenario->_player_positions[0];
  std::vector<btVector3> way;

  _player->initialize(_game->_scene, _physics, _game->_sound, _game->_input,
                      _game->_animation, _game->_gui, position, "player", way, 0);

  _human_controller = std::make_shared<HumanController>(
              _game->_input, 
              _player);
}

void
Session::initialize_queen() {
  btVector3 position = _scenario->_queen_position;
  _queen->initialize(_game->_scene, _physics, _game->_sound, _game->_input,
                     _game->_animation, _game->_gui, position, "queen");
}

void
Session::initialize_enemies() {
  btVector3 position;
  std::stringstream name;
  Player::shared enemy;
  AIController::shared ai_controller;
  std::vector<btVector3> way;
  //int max_respawn_times = _scenario->_number_of_enemies /
  //                        _scenario->_enemy_sources_positions.size();
  int max_respawn_times = 1;
  int player_per_fount = 2;
  int delay;
  for (int i = 0; 
        i < (int) _scenario->_enemy_sources_positions.size(); 
        i++) {
    position = _scenario->_enemy_sources_positions[i];
    way = calculate_way(position);
    for(int j = 0; j < player_per_fount; j++) {
      name.str("");
      name << "enemy" << i << j;
      enemy = std::make_shared<Player>();
      enemy->initialize(_game->_scene, _physics, _game->_sound, _game->_input,
                        _game->_animation, _game->_gui, position, name.str(), way,  
                        max_respawn_times);
      delay = i * 2 + j * 10;
      ai_controller = std::make_shared<AIController>(enemy, delay);
      _no_human_controllers.push_back(ai_controller);
    }
  }
}

std::vector<btVector3>
Session::calculate_way(btVector3 initial_position) {
  std::vector<btVector3> way;
  btVector3 destination = _scenario->_queen_position;

  int distance = std::abs(destination.x() - initial_position.x()) +
                 std::abs(destination.z() - initial_position.z());
  distance = distance / 20;

  btVector3 last_point = initial_position;
  for(;distance > 0; distance--) {
    last_point = _physics->closest_point(
            last_point, _scenario->_distace_tree[distance]);
    way.push_back(last_point);  
  }

  return way;
}

bool
Session::all_enemies_lose() {
  bool all_lose = true;
  for(auto& controller: _no_human_controllers) { 
    all_lose = all_lose && controller->_player->has_lose();
    if(!all_lose) break;
  }
  return all_lose;
}

void
Session::pause() {
  _paused = true;
  _player->frost();
  _player->_power_bar->setVisible(false);
  for(auto& controller: _no_human_controllers)
    controller->_player->frost();
}

void
Session::add_collision_hooks() {
  for(auto& controller: _no_human_controllers)
    controller->add_collision_hooks();
  _human_controller->add_collision_hooks();
  _queen->add_collision_hooks();
}

void
Session::show_interface() {
  initialize_overlays();
  _player->_power_bar->setVisible(true);
}

void
Session::set_camera() {
  _game->_scene->switch_next_camera();
}

void
Session::register_keybindings(){
  _game->_input->add_hook({OIS::KC_9, EventTrigger::OnKeyPressed},
                          EventType::doItOnce,
                          std::bind(&Session::set_camera, this));             
}

void
Session::update(float deltaT) {
  if(!_started) return;

  _physics->step_simulation(deltaT, 128);
  _physics->check_collision();
  _game->_animation->update(deltaT);
  
  for(auto& controller: _no_human_controllers)
    controller->update(deltaT);

  _human_controller->update(deltaT);
  _queen->update(deltaT);

  if(_queen->has_lose()) _player->_died = true;
  
  if(_player->has_lose() || all_enemies_lose())
    _finished = true;

  update_overlays();
}