#ifndef PLAYABLE_CHARACTER_HPP
#define PLAYABLE_CHARACTER_HPP

#include <memory>
#include <cmath>

#include <OgreEntity.h>
#include <OgreSceneNode.h>

#include "scene.h"
#include "physics.h"
#include "sound.h"
#include "input.h"
#include "animation.h"

#include "gui.h"
#include "ball.h"
#include "engine.h"

class Player {

  Scene::shared _scene;
  Physics::shared _physics;
  Sound::shared _sound;
  EventListener::shared _input;
  Animation::shared _animation;
  GUI::shared _gui;
  
  int _number_of_balls;
  int _max_respawn_times, _left_respawn_times;
  std::vector<std::shared_ptr<Ball>> _balls;
  int _ball_index;
  int _offset;
  Ogre::AnimationState* _walking;
  Ogre::AnimationState* _attacking;
  Ogre::AnimationState* _dead;

  btVector3 _origin;
  Ogre::SceneNode* _node;
  Ogre::SceneNode* _shoot_position;

  float _deltaT;
  float _shoot_delay, _lose_life_delay, _respawn_delay;
  bool _frozen, _init, _is_attacking;
  std::map<Engine::Action, std::function<void()>> _action_hooks;

 public:
  typedef std::shared_ptr<Player> shared;

  Ogre::SceneNode* _first_person_camera;
  Ogre::SceneNode* _third_person_camera;
  btRigidBody* _body;
  std::string _name;

  std::vector<btVector3> _way_points;
  CEGUI::Window* _power_bar;

  int _index;

  int _level;
  int _health_points, _max_health_points;
  bool _near_to_enemy, _died;

  float _score, _time_counter;
  Player();
  ~Player();

  void initialize(Scene::shared scene, Physics::shared physics,
                  Sound::shared sound, EventListener::shared input,
                  Animation::shared animation, GUI::shared gui,
                  btVector3 origin, std::string name,
                  std::vector<btVector3> way_points = std::vector<btVector3>(), 
                  int max_respawn_times = 0);

  void add_action_hooks();
  void add_collision_hooks();

  void exec(Engine::Action action);

  void reset();
  void respawn();
  void frost();
  void defrost();

  void follow_path();
  void rotate_to_point(btVector3 point);
  void rotate_to_enemy();
  void rotate_from_input();
  void rotate_horizontal(float x);
  void rotate(float x, float y);

  void move_up();
  void move_down();
  void move_right();
  void move_left();

  void shoot_weapon();

  void lose_life();

  void update(float deltaT);

  bool has_lose();
  bool has_win();

  void clear_counters();
  float get_time_counter();

  void next_level();

  std::string to_string();
  std::string time_to_string();
private:
	void initialize_character();
  void initialize_balls();
  void update_health_bar(float value);

	void create_traking_cameras();

  void shoot_callback();
  void move(btVector3 orientation);

  std::string to_string_with_precision(const float number, const int n);
};
#endif