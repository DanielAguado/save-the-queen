#include "scenario.h"
Scenario::Scenario() {
  
}

Scenario::Scenario(Scene::shared scene, Physics::shared physics,
                   Sound::shared sound, Animation::shared animation,
                   std::string scenario_filename){
  _x = _z = 0;
  _offset = 20;

	_scenario_filename = scenario_filename;
	_scene = scene;
	_physics = physics;
  _animation = animation;
  _sound = sound;

	_builder['|'] = std::bind(&Scenario::create_piece, this, "wall.mesh",
                  Orientation::VERTICAL);
  _builder['-'] = std::bind(&Scenario::create_piece, this, "wall.mesh", 
                  Orientation::HORIZONTAL);

  _builder[u'\u256D'] = std::bind(&Scenario::create_piece, this, "corner.mesh", 
                  Orientation::UP_LEFT);    // ╭
  _builder[u'\u256E'] = std::bind(&Scenario::create_piece, this, "corner.mesh", 
                  Orientation::UP_RIGHT);   // ╮
  _builder[u'\u256F'] = std::bind(&Scenario::create_piece, this, "corner.mesh", 
                  Orientation::DOWN_RIGHT); // ╯
  _builder[u'\u2570'] = std::bind(&Scenario::create_piece, this, "corner.mesh", 
                  Orientation::DOWN_LEFT);  // ╰

  _builder['e'] = [this] () { _enemy_sources_positions.push_back(
                  btVector3(_x * _offset, 2, _z * _offset)); };
  _builder['o'] = [this] () { 
                  _player_positions.push_back(
                    btVector3(_x * _offset, 2, _z * _offset)); };

  _builder['q'] = [this] () {_queen_position = btVector3(_x * _offset, 2, _z * _offset); };

  add_default_scenery();
	load();
  calculate_distance_tree();
}

Scenario::~Scenario() {

}

void
Scenario::clean() {
  if(_scenario_nodes.empty())
    return;

  for(auto node: _scenario_nodes)
    _scene->destroy_node(node);

  _scenario_nodes.clear();

  for(auto physic_body: _scenario_bodies) {
    _physics->remove_rigid_body(physic_body.first);
    delete physic_body.first;
    delete physic_body.second;
  }

  _scenario_bodies.clear();
  _enemy_sources_positions.clear();
  _player_positions.clear();

  _x = _z = 0;
}

void
Scenario::add_collision_hooks(btRigidBody* body, std::function<void()> callback) {
  for (auto piece: _scenario_bodies)
    _physics->add_collision_hooks(Physics::CollisionPair{body, piece.first}, callback);

}

void
Scenario::build_next(std::string file) {
	_scenario_filename = file;
  load();
}

void
Scenario::load() {
	read_file();
	parse();
  //_sound->play_fx("media/sound/forest_sound.wav");
}

void
Scenario::open_file() {
  std::cout << _scenario_filename << std::endl;
  _file.open(_scenario_filename);
  _file.imbue(std::locale(""));
  if (!_file.is_open()) {
    std::cerr << "no file: \n"
              << _scenario_filename
              << std::endl;
    exit(-1);
  }
}

void
Scenario::read_file(){
	open_file();

  std::wstring line;
	while(getline(_file, line))
		_scenario.push_back(line);

  _file.close();
}

void
Scenario::create_piece(std::string mesh, Scenario::Orientation orientation) {
  int pos_x = _x * _offset;
  int pos_z = _z * _offset;

	std::stringstream name;
	name << mesh << _x << _z << orientation;

  Ogre::Entity* entity =  _scene->create_entity(name.str(), mesh);
  Ogre::SceneNode* node = _scene->create_graphic_element(entity, "", name.str());

  _scenario_nodes.push_back(node);

  btScalar mass(10000);
  btVector3 origin = btVector3(pos_x, 2, pos_z);
  btQuaternion rotation;

  switch(orientation) {
    case VERTICAL:
      rotation = btQuaternion(btVector3(1, 0, 0), btScalar(0));
      break;
    case HORIZONTAL:
      rotation = btQuaternion(btVector3(0, 1, 0), btScalar(1.57));
      break;
    case UP_LEFT:
      rotation = btQuaternion(btVector3(0, 1, 0), btScalar(-1.57));
      break;
    case UP_RIGHT:
      rotation = btQuaternion(btVector3(0, 1, 0), btScalar(3.14));
      break;
    case DOWN_LEFT:
      rotation = btQuaternion(btVector3(0, 1, 0), btScalar(0));
      break;
    case DOWN_RIGHT:
      rotation = btQuaternion(btVector3(0, 1, 0), btScalar(1.57));
      break;
  }

  btTransform transformation = btTransform(rotation, origin);
  btCollisionShape* shape = _physics->create_shape(
       new MeshStrider(entity->getMesh().get()));

  btRigidBody* body = _physics->create_rigid_body(transformation, node, shape, mass);
  _scenario_bodies.push_back({body, shape});

  _physics->add_body_to_group(body, "scenario_collisions");
}

void
Scenario::parse() {
  _number_of_enemies = std::max(_scenario.size(), _scenario[0].size());
  _z = 0;
	for(std::wstring& tier: _scenario) {
		_z++;
		_x = 0;

		for(auto brick: tier) {
			_x++;
      if(_builder[brick])
        _builder[brick]();
		}
	}
}

void
Scenario::calculate_distance_tree() {
  int distance = 0;
  btVector3 point;
  _z = 0;
  for(std::wstring& tier: _scenario) {
    _z++;
    _x = 0;

    for(auto brick: tier) {
      _x++;
      if(brick == ' ') {
        point = btVector3(_x * _offset, 2, _z * _offset);
        distance = std::abs(point.x() - _queen_position.x()) +
                   std::abs(point.z() - _queen_position.z());
        distance = distance / _offset;
        _distace_tree[distance].push_back(point);
      }
    }
  }
}

void
Scenario::add_default_scenery() {
  Ogre::SceneNode* ground_node =
    _scene->create_plane("Y", "ground", "Ground", "", "Ground");

  btCollisionShape* groundShape =
    _physics->create_shape(btVector3(1000, 0, 1000));
  btRigidBody* plane_body =
    _physics->create_rigid_body(btTransform(btQuaternion(0, 0, 0, 1),
                         btVector3(0, 1, 0)),ground_node, groundShape, 0);
  plane_body->setRestitution(0.2);
  plane_body->setFriction(0.5f);
}

void
Scenario::add_entity(std::string name, std::string mesh,
                     Ogre::Vector3 position, float scale) {
  
  Ogre::Entity* entity =  _scene->create_entity(name, mesh, false);
  Ogre::SceneNode* node = _scene->create_graphic_element(entity, "", name);
  
  node->setPosition(position);
  node->setScale(Ogre::Vector3(scale, scale, scale));
}
