#ifndef SESSION_HPP
#define SESSION_HPP
#include <memory>
#include <sstream>

#include "physics.h"
#include "meshstrider.h"
#include "scenario.h"
#include "player.h"

#include "human_controller.h"
#include "ai_controller.h"

class Game;

class Session {
  bool _started;
  int _camera_index;
  Scenario::shared _scenario;  
  std::shared_ptr<Game> _game;
  Physics::shared _physics;

  std::vector<Controller::shared> _no_human_controllers;

  Player::shared _queen;
  Player::shared _player;
  HumanController::shared _human_controller;

  Ogre::OverlayElement* _timer_overlay;

 public:
  typedef std::shared_ptr<Session> shared;
  bool _finished, _paused;

  Session(std::shared_ptr<Game> game);
  ~Session();

  void initialize();
  
  void reset();
  void pause();
  void resume();

  void update(float deltaT);
  void show_interface();

  void clean_session();
  void register_keybindings();

 private:
  void initialize_scenario();

  void initialize_enemies();
  void initialize_player();
  void initialize_queen();

  void initialize_overlays();
  void update_overlays();
  void reset_overlays();
  void show_overlays();
  
  std::vector<btVector3> calculate_way(btVector3 initial_position);
  bool all_enemies_lose();

  void add_collision_hooks();
  void set_camera();

  std::string to_string_with_precision(const float number, const int n);
};
#endif
