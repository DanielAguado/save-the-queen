#include "ball.h"

Ball::Ball() {

}

Ball::~Ball(){

}

void
Ball::initialize(Scene::shared scene, Physics::shared physics, Sound::shared sound,
                 btVector3 origin, std::string name) {
  _physics = physics;
  _sound = sound;
  _origin = origin;
  _scene = scene;
  _shooting = false;
  _name = name;

  int mass = 1;

  std::stringstream complete_name;
  complete_name << _name << origin.x() << origin.y() << origin.z() << "ball";

  Ogre::Entity* entity = _scene->create_entity(complete_name.str(), 
                         "Sphere.mesh");
  Ogre::SceneNode* node = _scene->create_graphic_element(entity, "",
                         complete_name.str());
  node->setScale(Ogre::Vector3(0.5, 0.5, 0.5));
  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btTransform transformation(rotation, origin);
  btCollisionShape* shape = physics->create_shape(0.5);

  _body = physics->create_rigid_body(transformation, node, shape, mass);
  _body->setRestitution(1);
  _body->setLinearFactor(btVector3(1, 0, 1));
  _body->setAngularFactor(btVector3(0,0,0));
  _body->setGravity(btVector3(0, 0, 0));

  if (_name == "player")
    _physics->add_body_to_group(_body, "player_collisions");
  else
    _physics->add_body_to_group(_body, "enemy_collisions");
  
  _scene->create_particle(_scene->get_child_node(node, "ball_particle"),
                                 "ball_particle", "Bomb/ProjSmoke");
}

void
Ball::add_collision_hooks() {
  _physics->add_collision_hooks(_body, "scenario_collisions",
                                std::bind(&Ball::collide, this));

  if(_name == "player")
    _physics->add_collision_hooks(_body, "enemy_collisions",
                                std::bind(&Ball::collide, this));
  else
    _physics->add_collision_hooks(_body, "player_collisions",
                                std::bind(&Ball::collide, this));
}
void
Ball::reset() {
  _physics->absolute_move(_body, _origin);
  _shooting = false;
}

void
Ball::shot(Ogre::Vector3 position, btVector3 force) {
  _shooting = true;
  _physics->absolute_move(_body, btVector3(position.x, position.y, position.z));
  _body->applyCentralForce(force);

  _force = force;
}

void
Ball::collide() {
  Ogre::Vector3 position = _scene->convert_btvector3_to_vector3(_body->getCenterOfMassPosition());
  std::stringstream name;
  name << position.x << position.y << position.z << "particle";
  _scene->create_particle(name.str(), "Bomb/Explosion", position);
  _sound->play_fx("media/sound/explosion.wav");
  reset();
}

void
Ball::update() {
  _body->setLinearVelocity(btVector3(0, 0, 0));
  if (!_shooting)
    return;
  _body->applyCentralForce(_force);
}