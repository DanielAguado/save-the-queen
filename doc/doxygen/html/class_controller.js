var class_controller =
[
    [ "shared", "class_controller.html#affa7dad81e615837253382657c5c0efe", null ],
    [ "Controller", "class_controller.html#a2babc08de580c53785b0abe400431544", null ],
    [ "~Controller", "class_controller.html#a0ab87934c4f7a266cfdb86e0f36bc1b5", null ],
    [ "add_collision_hooks", "class_controller.html#a82931013d694453f754669d769055404", null ],
    [ "configure", "class_controller.html#ad4a5283d443ec0ca32f673f9686e026d", null ],
    [ "exec", "class_controller.html#a0c66320d327ca0b896c7a6dba7972ae0", null ],
    [ "update", "class_controller.html#aafde7f4a5ed5d3ec0d8ab26e3aef62f6", null ],
    [ "_player", "class_controller.html#a5f4bbdc1eab911862c25396205cdcee1", null ]
];