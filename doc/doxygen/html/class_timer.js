var class_timer =
[
    [ "DeltaTime", "class_timer.html#ab2dec2d408626a30262ab738036e55f1", null ],
    [ "Time", "class_timer.html#a01cf69cb36a17fcabf9844b0e248932e", null ],
    [ "Timer", "class_timer.html#a5f16e8da27d2a5a5242dead46de05d97", null ],
    [ "~Timer", "class_timer.html#a14fa469c4c295c5fa6e66a4ad1092146", null ],
    [ "get_delta_time", "class_timer.html#a0399534e9031e560a1f7da8867babf78", null ],
    [ "get_time_since_start", "class_timer.html#ada44b5917ec65beedbadf609e9a06313", null ],
    [ "start", "class_timer.html#a3a8b5272198d029779dc9302a54305a8", null ],
    [ "stop", "class_timer.html#a63f0eb44b27402196590a03781515dba", null ]
];