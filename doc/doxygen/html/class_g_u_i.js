var class_g_u_i =
[
    [ "shared", "class_g_u_i.html#abbd5eb22694e3e20dcd3c16a82bdea44", null ],
    [ "GUI", "class_g_u_i.html#a8cbb3140b7d3c9d8e942d6ce6b60a0e8", null ],
    [ "~GUI", "class_g_u_i.html#ac9cae2328dcb5d83bdfaeca49a2eb695", null ],
    [ "create_overlay", "class_g_u_i.html#ad0d7ce6f62f5debfd317b19bd99a6a3c", null ],
    [ "create_overlay", "class_g_u_i.html#aa5f9af2d8e88a0e0d81645e88add9fb7", null ],
    [ "get_context", "class_g_u_i.html#a4355585388d020886ad661e3f0bb3f67", null ],
    [ "inject_delta", "class_g_u_i.html#ae3fa83db5e1bb1403e932bf1aa53fd27", null ],
    [ "load_animation", "class_g_u_i.html#ace4b8423147ebcf2e83df2e70e7137c5", null ],
    [ "load_layout", "class_g_u_i.html#a4419faabc650e40b44d9847d3c769295", null ],
    [ "register_callback", "class_g_u_i.html#aef5da2d7413387c5e1a3c540f34dde21", null ],
    [ "switch_menu", "class_g_u_i.html#a399d0c53c36ecdf020d18759be50a925", null ]
];