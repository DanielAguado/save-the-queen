var class_game =
[
    [ "shared", "class_game.html#a79d7aca42091867f90d72213429722a8", null ],
    [ "Game", "class_game.html#ad59df6562a58a614fda24622d3715b65", null ],
    [ "~Game", "class_game.html#ae3d112ca6e0e55150d2fdbc704474530", null ],
    [ "enable_physics_debug_drawer", "class_game.html#a79758f35e0f02e7c1c4383673b38b350", null ],
    [ "has_ended", "class_game.html#a7e258a0db36d6421ea5658cf2b094ccc", null ],
    [ "init_SDL", "class_game.html#a4c43e15ba29a27c5e926241f57cc76d2", null ],
    [ "load_sound", "class_game.html#aea0dda050db5b38932363b7a875294fd", null ],
    [ "set_current_state", "class_game.html#a8a09ed13102779c3ec5393e0a86ca047", null ],
    [ "start", "class_game.html#a3d9b98f7c4a96ecf578f75b96c9f0e90", null ],
    [ "_animation", "class_game.html#a7f8f899697f06998714a503a8000675f", null ],
    [ "_debug_drawer", "class_game.html#af8e583139eb7ef2a133eb8f98e1d2fc6", null ],
    [ "_delta", "class_game.html#a6f445969bdb8aee4c36547200b9b5ecc", null ],
    [ "_gui", "class_game.html#a8b5d5158e18cb2b5f4458fe1cff19dbf", null ],
    [ "_input", "class_game.html#a9e2044e97b3795cb7cb5595becb5844c", null ],
    [ "_physics", "class_game.html#a327335c8294c1740db66a3ab875dfab9", null ],
    [ "_player", "class_game.html#a2efd5c8f660a13d00f195c55796b605b", null ],
    [ "_scene", "class_game.html#a21b476d559e1c6e12f1f019cdfdeebf8", null ],
    [ "_sound", "class_game.html#ad9dab788c92549cb4530f898ab08d375", null ]
];