var input_8h =
[
    [ "EventListener", "class_event_listener.html", "class_event_listener" ],
    [ "EventTrigger", "input_8h.html#afda2cc67fee8b0b8e9769cecede192cc", [
      [ "OnKeyPressed", "input_8h.html#afda2cc67fee8b0b8e9769cecede192cca084fdfa6842f962eaaab56928d0f196d", null ],
      [ "OnKeyReleased", "input_8h.html#afda2cc67fee8b0b8e9769cecede192ccab33c2b6c0bb5a630adeaf5fa024f4681", null ]
    ] ],
    [ "EventType", "input_8h.html#a2628ea8d12e8b2563c32f05dc7fff6fa", [
      [ "repeat", "input_8h.html#a2628ea8d12e8b2563c32f05dc7fff6faa32cf6da134a8b268cf4ab6b79a9a5ad9", null ],
      [ "doItOnce", "input_8h.html#a2628ea8d12e8b2563c32f05dc7fff6faa638b5b3da75dfb13dc6ec7c99a417070", null ]
    ] ]
];